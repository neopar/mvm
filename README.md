# Machines vs. Machines

A classic tower defense game.

Clone of Machines vs. Machines: https://launchpad.net/machines-vs-machines

## Building the game

You'll need to have Clickable installed: https://clickable-ut.dev/en/latest/

Then in order to build this you should be able to just run `clickable build`.

## Rebuilding level packs

Make sure you have the following dependencies:

`sudo apt install inkscape vorbis-tools`

Then you can re-build level packs by launching:

`./setupdata.py`

from within the `./data` directory.

## Download the latest builds

The repo is set to build click packages automatically for the following architectures:

- [Machines vs. Machines (arm64)](../-/jobs/artifacts/master/file/build/aarch64-linux-gnu/app/mvm_arm64.click?job=build-arm64)
- [Machines vs. Machines (armhf)](../-/jobs/artifacts/master/file/build/arm-linux-gnueabihf/app/mvm_armhf.click?job=build-armhf)
- [Machines vs. Machines (amd64)](../-/jobs/artifacts/master/file/build/x86_64-linux-gnu/app/mvm_amd64.click?job=build-amd64)
